defmodule State.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @registry :patient_registry

  @impl true
  def start(_type, _args) do
    children = [
      {State.PatientSupervisor, []},
      {Registry, [keys: :unique, name: @registry]},
      {State.Dispatcher, []}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: State.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
