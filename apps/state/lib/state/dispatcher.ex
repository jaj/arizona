defmodule State.Dispatcher do
  use GenServer
  require Logger
  alias State.{PatientSupervisor, Patient}

  @topics ["syringes"]

  ## GenServer API

  def start_link(name) do
    GenServer.start_link(__MODULE__, name, name: __MODULE__)
  end

  ## GenServer Callbacks

  @impl true
  def init(_) do
    for topic <- @topics do
      Phoenix.PubSub.subscribe(Dashboard.PubSub, topic)
    end

    {:ok, []}
  end

  @impl true
  def handle_info([syringe_data: data], state) do
    # Dispatcher received syringe: [timestamp: ~N[2013-05-17 21:46:54], clinicalunit: "My Unit", bedlabel: "Bed4", device: "2", attributes: %{"bolused" => "1.21", "drug" => "\"\"", "rate" => "0.10", "volume" => "35.63"}]
    Logger.debug("Dispatcher received syringe: #{inspect(data)}")
    PatientSupervisor.start_child(data[:patientid])
    Patient.update_syringe_data(data[:patientid], data)
    {:noreply, state}
  end

  ## Private Functions
end
