defmodule SyringePump do
  defstruct deviceid: "", drug: "", data: [rate: [], volume: [], bolused: []]

  @type time_series :: [{DateTime, float}]

  @type t :: %SyringePump{
          deviceid: String.t(),
          drug: String.t(),
          data: [rate: time_series, volume: time_series, bolused: time_series]
        }
end

defmodule PatientData do
  defstruct patientid: "", clinicalunit: "", bedlabel: "", syringepumps: %{}

  @type device_id :: String.t()
  @type syringe_pumps :: %{device_id => SyringePump.t()}

  @type t :: %PatientData{
          patientid: String.t(),
          clinicalunit: String.t(),
          bedlabel: String.t(),
          syringepumps: syringe_pumps
        }
end

defmodule State.Patient do
  use GenServer
  require Logger
  alias Phoenix.PubSub

  @registry :patient_registry

  @type incoming_syringe_data :: [
          timestamp: DateTime,
          clinicalunit: String.t(),
          bedlabel: String.t(),
          device: String.t(),
          attributes: %{String.t() => String.t()}
        ]

  @type syringe_attributes :: %{String.t() => String.t()}

  ## GenServer API

  def start_link(name) do
    GenServer.start_link(__MODULE__, name, name: via_tuple(name))
  end

  def child_spec(process_name) do
    %{
      id: __MODULE__,
      start: {__MODULE__, :start_link, [process_name]},
      restart: :transient
    }
  end

  def update_syringe_data(process_name, data) do
    process_name |> via_tuple() |> GenServer.cast({:update_syringe_data, data})
  end

  ## GenServer Callbacks

  @impl true
  def init(name) do
    Logger.info("Starting Patient process #{name}")
    schedule_work()
    # TODO
    clinicalunit = ""
    bedlabel = ""
    patientid = "#{clinicalunit}_#{bedlabel}"
    patient = %PatientData{patientid: patientid, clinicalunit: clinicalunit, bedlabel: bedlabel}
    {:ok, patient}
  end

  @impl true
  def handle_cast({:update_syringe_data, incoming}, patient) do
    import Map

    Logger.debug("Received syringe data: #{inspect(incoming)}")

    device_id = incoming[:device]

    syringepumps =
      patient.syringepumps
      |> put_new(device_id, %SyringePump{deviceid: device_id})
      |> update!(device_id, fn s -> merge_syringe_attributes(s, incoming) end)

    {:noreply, %{patient | syringepumps: syringepumps}}
  end

  @impl true
  def handle_info(:send_data, patient) do
    syringe_data = reduce_syringe_data(patient.syringepumps)
    p = %{patient | syringepumps: syringe_data}
    # filter_old_data(patient, 10) |> IO.inspect()
    Logger.debug("Sending patient data to liveview: #{inspect(p)}")
    PubSub.broadcast(Dashboard.PubSub, "syringes_ui", syringe_data: p)

    Process.send_after(self(), :send_data, :timer.seconds(10))
    {:noreply, patient}
  end

  def handle_info(:send_graph, patient) do
    Process.send_after(self(), :send_graph, :timer.minutes(1))
    {:noreply, patient}
  end

  ## Private Functions

  defp via_tuple(name),
    do: {:via, Registry, {@registry, name}}

  defp schedule_work do
    Process.send_after(self(), :send_data, :timer.seconds(10))
    Process.send_after(self(), :send_graph, :timer.minutes(1))
  end

  ### Update State

  @spec merge_syringe_attributes(SyringePump.t(), incoming_syringe_data) :: SyringePump.t()
  defp merge_syringe_attributes(syringepump, incoming) do
    Enum.reduce(
      incoming[:attributes],
      syringepump,
      fn {attribute_id, value}, old_syringepump ->
        merge_attribute(
          old_syringepump,
          attribute_id,
          incoming[:timestamp],
          value
        )
      end
    )
  end

  @spec merge_attribute(SyringePump.t(), String.t(), DateTime, float) :: SyringePump.t()
  defp merge_attribute(syringepump, attribute_key, timestamp, value) do
    add_to_list = fn l ->
      case Float.parse(value) do
        {fv, _} -> [{timestamp, fv} | l]
        _ -> l
      end
    end

    case attribute_key do
      "drug" ->
        %{syringepump | drug: value}

      "rate" ->
        update_in(syringepump, [Access.key(:data), :rate], add_to_list)

      "volume" ->
        update_in(syringepump, [Access.key(:data), :volume], add_to_list)

      "bolused" ->
        update_in(syringepump, [Access.key(:data), :bolused], add_to_list)

      _ ->
        syringepump
    end
  end

  @spec filter_old_data(PatienData.t(), integer()) :: PatientData.t()
  defp filter_old_data(patient, age_seconds) do
    import Lens

    now = DateTime.now!("Etc/UTC")

    filt_data = fn data_list ->
      Enum.filter(
        data_list,
        fn {timestamp, _value} ->
          DateTime.diff(now, timestamp) <= age_seconds
        end
      )
    end

    path =
      key!(:syringepumps)
      |> map_values()
      |> key!(:data)
      |> keys!([:rate, :volume, :bolused])

    update_in(patient, [path], filt_data)
  end

  ### Build outgoing data

  @spec reduce_syringe_data(PatientData.syringe_pumps()) :: PatientData.syringe_pumps()
  defp reduce_syringe_data(syringe_pumps) do
    import Lens

    lens_syringe_data = map_values() |> key!(:data) |> keys!([:rate, :volume, :bolused])
    update_in(syringe_pumps, [lens_syringe_data], &reduce_indiv_syringe_data/1)
  end

  @spec reduce_indiv_syringe_data([{DateTime, String.t()}]) :: {nil | DateTime, nil | float}
  def reduce_indiv_syringe_data(l) do
    reduce_ts = fn
      [] ->
        nil

      ts ->
        ts
        |> Enum.map(fn t -> DateTime.to_unix(t, :millisecond) end)
        |> Statistics.mean()
        |> round
        |> DateTime.from_unix!(:millisecond)
    end

    reduce_vs = fn
      [] -> nil
      vs -> Statistics.mean(vs) |> Float.round(2)
    end

    {timestamps, values} = Enum.unzip(l)
    {reduce_ts.(timestamps), reduce_vs.(values)}
  end
end
