defmodule OruReader do
  @moduledoc """
  Documentation for `OruReader`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> OruReader.hello()
      :world

  """
  def hello do
    :world
  end
end
