defmodule OruReader.Dispatcher do
  @behaviour MLLP.Dispatcher
  alias OruReader.MessageHandler

  def generate_reply(message) do
    MLLP.Ack.get_ack_for_message(
      message,
      :application_accept
    )
    |> to_string()
    |> MLLP.Envelope.wrap_message()
  end

  def dispatch(:mllp_hl7, message, state) when is_binary(message) do
    import HL7.Query

    reply = generate_reply(message)
    message_type = message |> select("MSH") |> get_part("MSH-21.2")
    MessageHandler.parse_message(message_type, message)
    # PubSub.broadcast(Dashboard.PubSub, "syringes", syringe_data: res)
    {:ok, %{state | reply_buffer: reply}}
  end
end
