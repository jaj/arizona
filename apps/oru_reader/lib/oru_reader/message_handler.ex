defmodule OruReader.MessageHandler do
  import HL7.Query
  alias OruReader.LabelMapper
  alias Phoenix.PubSub

  defp get_syringe_id(seg) do
    subid = get_part(seg, "4")
    parts = subid |> String.split(".")
    # parts |> List.delete_at(-1) |> Enum.join(".")
    # parts[1]
    parts |> Enum.at(1)
  end

  defp extract_fields(seg) do
    field_identifiers = ["3.2", "5", "6.2"]
    fields_col = for p <- field_identifiers, do: get_parts(seg, p)
    syringe_numbers = for d <- get_data(seg), do: d.syringe
    [syringe_numbers | fields_col] |> List.zip()
  end

  defp fields_to_attribute_map(data) do
    import LabelMapper

    merge_unit = fn val, unit ->
      cond do
        unit == "" -> val
        # true -> "#{val} #{unit}"
        true -> val
      end
    end

    for {_devid, lbl, value, unit} <- data do
      %{map_label(lbl) => merge_unit.(value, map_unit(unit))}
    end
    |> Enum.reject(&(&1 |> Map.has_key?("")))
    |> Enum.reduce(&Map.merge/2)
  end

  @spec get_timestamp(HL7.Message.t()) :: Datetime.t()
  defp get_timestamp(message) do
    import Timex.Parse.DateTime.Parser, only: [parse!: 3]
    raw_msg = message |> HL7.Message.raw()
    timestr = raw_msg.header.message_date_time
    timestamp = parse!(timestr, "%Y%m%d%H%M%S", :strftime)
    timestamp |> DateTime.from_naive!("Etc/UTC")
  end

  defp get_location(message) do
    bed_label = message |> get_part("PV1-3.3")
    clinical_unit = message |> get_part("PV1-3.1")
    {clinical_unit, bed_label}
  end

  def parse_message("IHE PCD", message) do
    syringe_data =
      message
      |> select("OBR [{OBX}]")
      |> select("OBX")
      |> data(&%{syringe: get_syringe_id(&1)})
      |> extract_fields()
      # group by device id
      |> Enum.group_by(&(&1 |> elem(0)))
      # delete device 0 which bears no data
      |> Map.delete("0")
      # transform tuples of attributes to map
      |> Enum.map(fn {k, v} -> {k, fields_to_attribute_map(v)} end)

    {clinicalunit, bedlabel} = get_location(message)

    for {devid, attributes} <- syringe_data do
      message = [
        patientid: "#{clinicalunit}_#{bedlabel}",
        timestamp: get_timestamp(message),
        clinicalunit: clinicalunit,
        bedlabel: bedlabel,
        device: devid,
        attributes: attributes
      ]

      PubSub.broadcast(Dashboard.PubSub, "syringes", syringe_data: message)
    end
  end
end
