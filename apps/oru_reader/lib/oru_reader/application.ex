defmodule OruReader.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      {MLLP.Receiver,
       [
         port: 4090,
         dispatcher: OruReader.Dispatcher,
         packet_framer: MLLP.DefaultPacketFramer
       ]}
      # {Phoenix.PubSub, name: :hl7pubsub}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: OruReader.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
