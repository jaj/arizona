defmodule OruReader.LabelMapper do
  @label_map %{
    # "MDC_PUMP_STAT" => "status",
    # "NOM_SETT_PUMP_MODE" => "mode",
    "MDC_FLOW_FLUID_PUMP" => "rate",
    "MDC_VOL_FLUID_DELIV" => "volume",
    "MDC_VOL_FLUID_BOLUS" => "bolused",
    "NOM_SETT_DRUG_NAME_TYPE" => "drug",
    "MDC_DRUG_NAME_POINTER" => "drug"
  }
  @unit_map %{
    "MDC_DIM_MILLI_L" => "ml",
    "MDC_DIM_MILLI_L_PER_HR" => "ml/h"
  }

  def map_label(x) do
    Map.get(@label_map, x, "")
  end

  def map_unit(x) do
    Map.get(@unit_map, x, "")
  end
end
