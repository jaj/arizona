defmodule Dashboard.FooLive do
  use Dashboard, :live_view

  @topic "syringes_ui"

  def mount(_params, _session, socket) do
    Dashboard.Endpoint.subscribe(@topic)
    :timer.send_interval(1000, self(), :cleanup)
    {:ok, assign(socket, :syringepumps, %{})}
  end

  def handle_info([syringe_data: incoming], socket) do
    patientid = incoming.patientid

    merged =
      socket.assigns.syringepumps
      |> Map.put(patientid, incoming.syringepumps)

    IO.inspect(merged)
    {:noreply, assign(socket, :syringepumps, merged)}
  end

  def handle_info(:cleanup, socket) do
    # TODO
    {:noreply, socket}
  end
end
