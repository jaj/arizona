import Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :dashboard, Dashboard.Endpoint,
  http: [ip: {127, 0, 0, 1}, port: 4002],
  secret_key_base: "Om00FbkFXss6Mucwa4V7X40kJkBh4KBD25Ikqxm/NTQABFefszV19sUo1Z+goEQ7",
  server: false
